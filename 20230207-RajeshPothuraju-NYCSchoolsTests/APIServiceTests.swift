//
//  APIServiceTests.swift
//  20230207-RajeshPothuraju-NYCSchoolsTests
//
//  Created by Rajesh on 07/02/23.
//

@testable import _0230207_RajeshPothuraju_NYCSchools
import XCTest

class APIServiceTests: XCTestCase {

    var sut: APICaller!
    
    override func setUpWithError() throws {
        sut = APICaller()
    }

   func testGetSchool() {
        let expection = XCTestExpectation(description: "Schools data Downloaded") // API is asynchrouns operation so that's why we are using XCTestExpectation to be able to wait untill finishing
       var responseError: Error?
       var schools: [School]?
       
       APICaller.getSchools { result in
           //self?.isLoadingData.value = false
           
           switch result {
           case .success(let schoolsData):
               schools = schoolsData
           case .failure(let err):
               print(err)
               responseError = err
           }
           expection.fulfill()
       }
       
       wait(for: [expection], timeout: 1)
       XCTAssertNil(responseError)// Make sure that error == nil
       XCTAssertNotNil(schools) // Make sure that response != nil
       
    }
    
    func testGetSchoolDetails() {
         let expection = XCTestExpectation(description: "Schools data Downloaded") // API is asynchrouns operation so that's why we are using XCTestExpectation to be able to wait untill finishing
        var responseError: Error?
        var schoolDetails: [SchoolDetails]?
        
        APICaller.getSchoolDetails { result in
            //self?.isLoadingData.value = false
            
            switch result {
            case .success(let schoolDetailsData):
                schoolDetails = schoolDetailsData
            case .failure(let err):
                print(err)
                responseError = err
            }
            expection.fulfill()
        }
        
        wait(for: [expection], timeout: 1)
        XCTAssertNil(responseError)// Make sure that error == nil
        XCTAssertNotNil(schoolDetails) // Make sure that response != nil
        
     }
    
    
    override func tearDownWithError() throws {
        sut = nil
    }

}


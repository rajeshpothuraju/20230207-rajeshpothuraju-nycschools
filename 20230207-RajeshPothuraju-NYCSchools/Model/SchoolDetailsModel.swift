//
//  SchoolDetailsModel.swift
//  20230207-RajeshPothuraju-NYCSchools
//
//  Created by Rajesh on 07/02/23.
//

import Foundation

struct SchoolDetails: Codable {
    
    let id: String?
    let name: String?
    let numOfTestTakers: String?
    let satCriticalReadingAvgScore: String?
    let satMathAvgScore: String?
    let satWritingAvgScore: String?

    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case numOfTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}

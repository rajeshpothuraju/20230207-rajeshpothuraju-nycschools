//
//  SchoolModel.swift
//  20230207-RajeshPothuraju-NYCSchools
//
//  Created by Rajesh on 07/02/23.
//

import Foundation

struct School: Codable {
    
    let id: String?
    let name: String?
    let location: String?

    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case location 
    }
}


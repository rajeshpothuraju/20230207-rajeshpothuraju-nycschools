//
//  NetworkConstants.swift
//  MovieDetailsMVVM
//
//  Created by Sajjad Sarkoobi on 26.06.2022.
//

import Foundation

class NetworkConstants {
    
    public static var shared: NetworkConstants = NetworkConstants()
    
    public var schoolsURL: String {
        get {
            return "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        }
    }
    
    public var schoolDetailsURL: String {
        get {
            return "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        }
    }
}

//
//  AppDelegate.swift
//  20230207-RajeshPothuraju-NYCSchools
//
//  Created by Rajesh on 07/02/23.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//
//        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
//
//        let rootViewController = mainStoryboard.instantiateViewController(withIdentifier: "SchoolsViewController") as? SchoolsViewController
//        let navigationController = UINavigationController(rootViewController: rootViewController!)
//
//        self.window!.rootViewController = navigationController
//
//        self.window!.makeKeyAndVisible()
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        let mainNavigationController = UINavigationController(rootViewController: SchoolsViewController())
        window.rootViewController = mainNavigationController
        window.makeKeyAndVisible()
        
        self.window = window
        
        return true
    }

    

}


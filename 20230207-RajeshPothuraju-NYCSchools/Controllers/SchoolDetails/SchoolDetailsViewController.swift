//
//  SchoolDetailsViewController.swift
//  20230207-RajeshPothuraju-NYCSchools
//
//  Created by Rajesh on 07/02/23.
//

import UIKit

class SchoolDetailsViewController: UIViewController {

    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var testTakersLabel: UILabel!
    @IBOutlet weak var satReading_avg_score: UILabel!
    @IBOutlet weak var satMathAvgScore: UILabel!
    @IBOutlet weak var satWritingAvgScore: UILabel!
    
    
    //ViewModel
    var viewModel: SchoolsViewModel = SchoolsViewModel()
    
    //Variables:
    var selectedSchool: SchoolDetails?

    override func viewDidLoad() {
        super.viewDidLoad()
                
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateSchoolDetails()
    }
    
    
    private func updateSchoolDetails() {
        
        schoolName.text = selectedSchool?.name ?? ""
        
        testTakersLabel.text = "Test Takers: \(selectedSchool?.numOfTestTakers ?? "")"
        satReading_avg_score.text = "SAT Reading Avg: \( selectedSchool?.satCriticalReadingAvgScore ?? "")"
        satMathAvgScore.text = "SAT Math Avg: \(selectedSchool?.satMathAvgScore ?? "")"
        satWritingAvgScore.text = "SAT Writing Avg: \(selectedSchool?.satWritingAvgScore ?? "")"
    }

}

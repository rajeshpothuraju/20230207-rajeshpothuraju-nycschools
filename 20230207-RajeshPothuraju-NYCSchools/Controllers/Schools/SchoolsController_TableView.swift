//
//  SchoolsController_TableView.swift
//  20230207-RajeshPothuraju-NYCSchools
//
//  Created by Rajesh on 07/02/23.
//

import Foundation
import UIKit

extension SchoolsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setupTableview() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
                
        registerCells()
    }
    
    func reloadTableView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func registerCells(){
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = schoolsDataSource[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        guard let schoolId = schoolsDataSource[indexPath.row].id else { return }
        guard let selectedSchoolDetails = viewModel.retriveSchoolDetails(withId: schoolId)
        else {
            showNoDetailsAlert()
            return
            
        }
        
        let detailsVC = SchoolDetailsViewController()
        detailsVC.selectedSchool = selectedSchoolDetails
        self.navigationController?.pushViewController(detailsVC, animated: true)
    }
}

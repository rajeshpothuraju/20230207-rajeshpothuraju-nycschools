//
//  SchoolsViewController1.swift
//  20230207-RajeshPothuraju-NYCSchools
//
//  Created by Rajesh on 07/02/23.
//

import UIKit

class SchoolsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //ViewModel
    var viewModel: SchoolsViewModel = SchoolsViewModel()
    
    //Variables:
    var schoolsDataSource: [School] = []
    var schoolDetailsDataSource: [SchoolDetails] = []
    var selectedSchoolDetails: SchoolDetails?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configurView()
    }
    
    func configurView(){
        self.title = "NYC Schools"
        setupTableview()
        bindViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if schoolsDataSource.isEmpty {
            viewModel.getData()
        }
        
    }
    
    func bindViewModel() {
        viewModel.isLoadingData.bind { [weak self] isLoading in
            guard let isLoading = isLoading else {
                return
            }
            DispatchQueue.main.async {
                if isLoading {
                    self?.activityIndicator.startAnimating()
                } else {
                    self?.activityIndicator.stopAnimating()
                }
            }
        }
        
        viewModel.schools.bind { [weak self] schools in
            guard let self = self,
                  let schools = schools else {
                return
            }
            self.schoolsDataSource = schools
            self.reloadTableView()
        }
        
        viewModel.schoolDetails.bind { [weak self] schoolDetails in
            guard let self = self,
                  let schoolDetails = schoolDetails else {
                return
            }
            self.schoolDetailsDataSource = schoolDetails
        }
    }
    
    func showNoDetailsAlert() {
    
        let alert = UIAlertController(title: "Info", message: "School details not found.", preferredStyle: UIAlertController.Style.alert)

        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

        // show the alert
        self.present(alert, animated: true, completion: nil)
        
    }
}

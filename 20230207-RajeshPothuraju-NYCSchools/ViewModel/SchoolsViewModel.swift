//
//  SchoolsViewModel.swift
//  20230207-RajeshPothuraju-NYCSchools
//
//  Created by Rajesh on 07/02/23.
//

import Foundation

class SchoolsViewModel {
    
    var isLoadingData: Observable<Bool> = Observable(false)
    var dataSource: [School]?
    var schoolDetailsDataSource: [SchoolDetails]?
    var schools: Observable<[School]> = Observable(nil)
    var schoolDetails: Observable<[SchoolDetails]> = Observable(nil)
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRows(in section: Int) -> Int {
        return dataSource?.count ?? 0
    }
    
    func getData() {
        if isLoadingData.value ?? true {
            return
        }
        
        let dispatchGroup = DispatchGroup()
        
        dispatchGroup.enter()
        
        isLoadingData.value = true
        APICaller.getSchools { [weak self] result in
            //self?.isLoadingData.value = false
            
            switch result {
            case .success(let schoolsData):
                self?.dataSource = schoolsData
                self?.mapSchoolData()
            case .failure(let err):
                print(err)
            }
            
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        
        APICaller.getSchoolDetails { [weak self] result in
            //self?.isLoadingData.value = false
            
            switch result {
            case .success(let schoolDetailsData):
                self?.schoolDetailsDataSource = schoolDetailsData
                self?.mapSchoolDetailsData()
            case .failure(let err):
                print(err)
            }
            
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main){
            self.isLoadingData.value = false
        }
        
        
    }
    
    private func mapSchoolData() {
        schools.value = self.dataSource?.compactMap({$0})
    }
    
    private func mapSchoolDetailsData() {
        schoolDetails.value = self.schoolDetailsDataSource?.compactMap({$0})
    }
    
    func retriveSchoolDetails(withId id: String) -> SchoolDetails? {
        guard let schoolDetails = schoolDetailsDataSource?.first(where: {$0.id == id}) else {
            return nil
        }
        
        return schoolDetails
    }
    
}
